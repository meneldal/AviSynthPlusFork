// Avisynth v2.5.  Copyright 2002 Ben Rudiak-Gould et al.
// http://www.avisynth.org

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA, or visit
// http://www.gnu.org/copyleft/gpl.html .
//
// Linking Avisynth statically or dynamically with other modules is making a
// combined work based on Avisynth.  Thus, the terms and conditions of the GNU
// General Public License cover the whole combination.
//
// As a special exception, the copyright holders of Avisynth give you
// permission to link Avisynth with independent modules that communicate with
// Avisynth solely through the interfaces defined in avisynth.h, regardless of the license
// terms of these independent modules, and to copy and distribute the
// resulting combined work under terms of your choice, provided that
// every copy of the combined work is accompanied by a complete copy of
// the source code of Avisynth (the version of Avisynth used to produce the
// combined work), being distributed under the terms of the GNU General
// Public License plus this exception.  An independent module is a module
// which is not derived from or based on Avisynth, such as 3rd-party filters,
// import and export plugins, or graphical user interfaces.

// RemoveDuplicate

#include <avisynth.h>
#include <cstdint>
#include <vector>
#include <numeric>
#include <fstream>
#include <iomanip>
#include <string>

class RemoveDuplicate : public GenericVideoFilter
{
public:

	RemoveDuplicate(PClip src, bool firstpass, double threshold, const char* msefile, const char* timecodefile, IScriptEnvironment* env) : GenericVideoFilter(src),
		firstpass(firstpass), threshold(threshold), msefile(msefile), timecodefile(timecodefile)
	{
		if (!firstpass)
			InitFrameMap(env);
	}

	void InitFrameMap(IScriptEnvironment* env) {
		std::ifstream infile(msefile);
		if (!infile)
			env->ThrowError("RemoveDuplicate: Error: Can't open mse file for reading");
		double d;
		for (int inputframe = 0; infile >> d; ++inputframe)
		{
			if (inputframe == 0) {
				frame_index.push_back({ 0,1 });
				continue;
			}
			if (d < threshold) {
				//remove that frame
				frame_index.back().second++;
			}
			else {
				frame_index.push_back({ inputframe,1 });
			}
		}
		std::ofstream outfile(timecodefile);
		if (!outfile)
			env->ThrowError("RemoveDuplicate: Error: Can't open timecode file for writing");
		int framecounter = 0;
		outfile << std::fixed << std::setprecision(2);
		outfile << "# timecode format v2\n";
		for (auto it = frame_index.begin(); it != frame_index.end(); ++it) {
			double frametime = (framecounter * (double)vi.fps_denominator * 1000.0) / (double)vi.fps_numerator;
			outfile << frametime << "\n";
			framecounter += it->second;
		}
		if (vi.num_frames != frame_index.size()) {

			int i = std::gcd(vi.num_frames, frame_index.size());
			int num_f = frame_index.size() / i;
			int den_f = vi.num_frames / i;
			while ((double)vi.fps_numerator * double(num_f) > double(INT_MAX)) {
				num_f >>= 1;
				den_f >>= 1;
			}
			vi.fps_numerator *= num_f;
			vi.fps_denominator *= den_f;
		}
		vi.num_frames = frame_index.size();
	}

	PVideoFrame __stdcall GetFrame(int n, IScriptEnvironment* env)
	{
		if (firstpass) {
			PVideoFrame src = child->GetFrame(n, env);
			if (n == 0) { //no frame to compare it to
				if (mse.empty())
					mse.push_back(0);
				return src;
			}
			else if (n < mse.size()) {//already computed that frame

			}
			else if (n > mse.size()) {	// Didn't compute previous frame
				env->ThrowError("RemoveDuplicate: Error: frames were skipped during processing");
			}
			//new frame, nothing missing
			PVideoFrame prev = child->GetFrame(n - 1, env);
			const unsigned char* psrc = src->GetReadPtr(PLANAR_Y);
			const unsigned int pitch = src->GetPitch(PLANAR_Y);
			const unsigned int row_size = src->GetRowSize(PLANAR_Y);
			const unsigned int height = src->GetHeight(PLANAR_Y);
			const unsigned char* pprev = prev->GetReadPtr(PLANAR_Y);
			const unsigned int prevpitch = prev->GetPitch(PLANAR_Y);
			__int64 err = 0;
			for (auto i = 0U; i < height; ++i) {
				err += std::transform_reduce(psrc, psrc + row_size, pprev, __int64(0), std::plus(), [](auto x, auto y) {return (abs(x - y) * abs(x - y)); });
				psrc += pitch;
				pprev += prevpitch;
			}
			mse.push_back(((double)err) / ((double)row_size * (double)height));
			if (n == vi.num_frames - 1) {
				//print the mse values to disk
				std::ofstream outfile(msefile);
				if (!outfile)
					env->ThrowError("RemoveDuplicate: Error: Can't open mse file for writing");
				for (auto d : mse) {
					outfile << d << "\n";
				}
			}
			return src;
		}
		else {
			return child->GetFrame(frame_index[n].first, env);
		}
	}

	static AVSValue __cdecl Create(AVSValue args, void*, IScriptEnvironment* env)
	{
		PClip clip = args[0].AsClip();
		bool firstpass = args[1].AsBool(true);
		double threshold = args[2].AsFloat(20.0);
		const char* msefile = args[3].AsString("mse.txt");
		const char* timecodefile = args[4].AsString("timecodes.txt");
		return new RemoveDuplicate(clip, firstpass, threshold, msefile, timecodefile, env);
	}
private:
	const bool firstpass;
	const double threshold;
	const char* msefile;
	const char* timecodefile;
	std::vector<double>mse;
	std::vector<std::pair<int, int>>frame_index;
};

const AVS_Linkage* AVS_linkage = 0;
extern "C" __declspec(dllexport) const char* __stdcall AvisynthPluginInit3(IScriptEnvironment * env, const AVS_Linkage* const vectors) {
	AVS_linkage = vectors;

	env->AddFunction("RemoveDuplicate", "c[firstpass]b[threshold]f[msefile]s[timecodefile]s",
		RemoveDuplicate::Create, 0);
	// The AddFunction has the following paramters:
	// AddFunction(Filtername , Arguments, Function to call,0);

	// Arguments is a string that defines the types and optional nicknames of the arguments for you filter.
	// c - Video Clip
	// i - Integer number
	// f - Float number
	// s - String
	// b - boolean
	// . - Any type (dot)
	//      Array Specifiers
	// i* - Integer Array, zero or more
	// i+ - Integer Array, one or more
	// .* - Any type Array, zero or more
	// .+ - Any type Array, one or more
	//      Etc

	// A freeform name of the plugin.
	return "`RemoveDuplicate' Remove highly simmilar successive frames.";
}
